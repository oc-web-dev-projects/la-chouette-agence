# La Chouette Agence

4th project of the OpenClassrrooms' path: Web Developer

> Optimize an Existing Website
Research and implement current best practices and standards in web development, including SEO, size and speed, and accessibility.

🔧 Skills acquired in this project:

- Research web development best practices
- Optimize the size and speed of a website
- Analyze the search engine performance of a website
- Write current, maintainable code in HTML & CSS
- Ensure the accessibility of a website according to WCAG2.0